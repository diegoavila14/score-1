//Aqui van todas las formulas que sirven para simular la fisica del juego, son los "comandos"
public class Simulation {

	public Simulation() {}
	
	public double startingAX(double angle , double force , int weight)
	{
		return ((Math.cos(angle)*force)/weight);
	}
	
	public double startingAY(double angle , double force , int weight)
	{
		return ((Math.sin(angle)*force)/weight);
	}

}
