import java.awt.Point;
public class Ball {

	/**
	 * @param args
	 */
	public int weight;
	
	public String name;
	
	public Point position; 
	
	public Ball(int weight , String name , int x , int y)
	{
		this.weight=weight;
		this.name=name;
		
		position=new Point(x , y);
		
	}
}
