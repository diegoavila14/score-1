import java.awt.*;

//esta clase modela los objetos que ser�n lanzados.
public class MyObject {

	public double mass;
	public int gravity;
	public int velocity;
	public int angle;
	public Point position;
	public Point inicialPosition;
	
	public MyObject(double m, int g, int v, int a, Point p)
	{
		mass = m;
		gravity = g;
		velocity = v;
		angle = a;
		position = p;
		inicialPosition = p;
		
	}
	
	public void setWeight(int mass) {
		this.mass = mass;
	}

	public void setGravity(int gravity) {
		this.gravity = gravity;
	}

	public void setPosition(Point position) {
		this.position = position;
	}
	
	
	/**
	 * este m�todo permite generar un informe de las caracter�sticas del objeto
	 * @return informe con las caracter�sticas (string)
	 */
	public String informeCaracteristicas()
	{
		return "<html><pre>Caracter�sticas:"+
	"\n�ngulo: "+angle+" grados"+
	"\nVelocidad: "+velocity+" m/s�"+
	"\nGravedad: "+gravity+" m/s�"+
	"\nMasa: "+mass+" kg"+
	"\nPosici�n: ("+((int)position.getX()+", "+(int)position.getY())+ ")</pre></html>";
		
	}
	
	
	
}
